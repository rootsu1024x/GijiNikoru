<?php
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

	$mid = $_POST['m'];
	$cid = $_POST['c'];

	// NIJを取得する
	$nij = file_get_contents("./gijiNikoru.json");
	
	// オブジェクト化
	$nijObj = json_decode($nij, true);

	// まだNIMCJが存在しない場合、初期化する
	if (!$nijObj[$mid]) {
		$nijObj[$mid] = array();
	}

	// NIMCJを取得
	$nimcj = $nijObj[$mid][$cid];
	
	// NIMCJを更新する
	if (!$nimcj) {
		$nijObj[$mid][$cid] = 1;
	} else {
		$nijObj[$mid][$cid]++;
	}

	$jj = json_encode($nijObj, true);

	file_put_contents("./gijiNikoru.json", $jj);
