<?php
	function getNimj($mid) {

		// 接続する
		$mysqli = new mysqli("mysql103.phy.lolipop.lan", "user", "password", "database");


		// 接続状況をチェックする
		if ($mysqli->connect_errno) {
		    return json_encode(["error"=>"db_connect_failed","description"=>$mysqli->connect_error])
		}

		$myArray = array();

		$query = "SELECT comment_id AS cid, nikorare AS n FROM nikorare WHERE movie_id = '$mid'";
		if ($result = $mysqli->query($query)) {

			while($row = $result->fetch_array(MYSQL_ASSOC)) {
				$myArray[$row['cid']] = $row['n'];
			}
		}

		// クローズ
		$result->close();
		$mysqli->close();

		if (count($myArray) > 0) {
			return json_encode($myArray);
		}
		return null;
	}

	// ヘッダ設定
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
	// var_dump(headers_list());

	// リクエストから動画IDを取得する
	$mid = $_GET['a'];

	// NIMJをDBから取得して返却する
	echo( getNimj($mid) );
