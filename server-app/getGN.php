<?php
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

	// var_dump(headers_list());

	// リクエストからコメ番を取得する
	$movieId = $_GET['a'];
	
	// NIJを取得する
	$json = file_get_contents("./gijiNikoru.json");
	
	// オブジェクト化
	$data = json_decode($json, true);
	
	// NIMJを取得
	$movieInfo = $data[$movieId];
	
	// 存在しなかった場合は空オブジェクトを返す
	if (!$movieInfo) {
		$movieInfo = json_decode("{}");
	}
	
	// JSON化
	$movieInfoJSON = json_encode($movieInfo);
	
	// NIMJを返却する
	echo($movieInfoJSON);
