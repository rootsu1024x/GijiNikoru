const GN = (() => {

	const my = {};

	// ログレベル指定
	const logLevel = 0;

	// コメント監視間隔（ミリ秒）
	const commentObservationInterval = 1000;

	// 最大表示ニコられ数
	const maxDisplayNikorare = 999;

	// 縦ニコるくんURL
	const nikoruImage_tate = chrome.extension.getURL('images/nikoru_tate.png');

	// ニコり済みコメ番リスト
	let nikorizumiCIDList = [];

	// 動画番号
	let mid = null;

	// NIJ
	let myNimj = null;

	// グリッドカンバスDOM
	let gridCanvasDOM = null;

	// グリッドカンバスDOM監視処理チケット
	let canvasTicket = null;

	// 初期化処理
	my.init = function(movieId) {
		myLog(2, "would initialize GN...");

		// 初期化済みであれば何もしない（
		if (hasInitiated(movieId)) {
			myLog(2, "GN was already initialized. nothing to do.");

			// 既に実行済みなので実行しない
			return;
		}

		myLog(1, "initialize GN.");

		// 各種情報の初期化
		mid = movieId;
		myNimj = null;
		if (canvasTicket != null) {
			clearInterval(canvasTicket);
		}
		gridCanvasDOM = null;
		nikorizumiCIDList = [];

		// NIMJを取得する
		getNimj(onGotNimj);
	}

	// NIMJを取得する
	const getNimj = function (onGotNimj) {
		myLog(1, "get nimj...", {movieId: mid});
		$.getJSON("http://j9oi.xyz/getGN.php", {a: mid}, onGotNimj);
	};

	const onGotNimj = function(nimj, status) {
		myLog(1, "got nimj.", nimj);

		// 取得したNIMJを保存する
		myNimj = nimj;

		const ticket = setInterval(function() {
			myLog(1, "waiting grid-canvas appearance...");
			const gridCanvas = $(".comment-panel").find(".grid-canvas");

			if (gridCanvas && gridCanvas.length > 0) {
				myLog(1, "grid-canvas appeared.");
				gridCanvasDOM = gridCanvas;

				// マウスホイールでツールチップを削除する
				const mousewheelevent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
				gridCanvas.on(mousewheelevent,function(e){
					$("[role=tooltip]").remove();
				});

				// コメント枠内部の監視を開始する
				canvasTicket = setInterval(function() {
					canvasObserver();
				}, commentObservationInterval);

				// コメント枠存在監視を解除
				clearInterval(ticket);
			}
		}, 2000);
	};

	// DOM変更時処理
	const canvasObserver = function () {
		myLog(2, "canvasObserver---run...");

		// 処理付加済マークが付加されていないrowを取得する
		const rows = gridCanvasDOM.find(".slick-row:not(.nikoru)");

		if ( rows.length === 0 ) {
			myLog(2, "canvasObserver---target not found. nothing to do.");
			return;
		}

		// 処理付加済マークを付加する
		rows.addClass("nikoru");

		// 色付け
		setRowsStyle(rows);

		// ツールチップとニコる処理を付与する
		$.each(rows, (i, row) => {
			const commentId = $(row).find(".r3").html();
			const isNikorared = nikorizumiCIDList.includes(commentId);
			$(row).tooltip({
				show: false,
				hide: false,
				position: { my: "left-70 center", at: "left center" },
				items: ".r0",
				content: function() {
	 				return getNikoruDOM(commentId, isNikorared);
				}
			});

			if (isNikorared === false) {
				// ニコるイベントハンドラを付加する
				$(row).bind("dblclick", execNikoru);
			}
		});
	};

	// ニコる実行
	const execNikoru = function (e) {
		myLog(2, "dblclick---run.");

		// クリック対象を取得する
		const target = $(e.target);

		// 処理対象かを判定する
		const shouldProcess = target.hasClass("slick-cell") && target.hasClass("r0");

		if (shouldProcess === false) {
			myLog(2, "dblclick---this is not target.");
			return;
		}

		// 行DOMを取得する
		const row = target.parent();

		// コメ番を取得する
		const commentId = row.find(".r3").html();

		if (nikorizumiCIDList.includes(commentId)) {
			myLog(2, "dblclick---this comment already nikorare.");

			// ニコり済みの場合は何もしない
			return;
		}

		// ニコり済みコメ番リストに追加する
		nikorizumiCIDList.push(commentId);

		// ニコるくんを回転させる
		$(`#nikoruKun${commentId}`).addClass("nikoruKun_disabled");
		setTimeout(function(){
			// ニコるを縦=>横に更新する
			row.tooltip({
				content: function() {
					return getNikoruDOM(commentId, true);
				}
			});

			// 今回のニコりを加味してスタイルを更新する
			setRowStyle(row);
		},0.5*1000 /* 0.5秒 */);

		// シークさせない
		killEvent(e);

		myLog(1, "post...", {movieId: mid, commentId: commentId});

		// 送信する
		$.post("http://j9oi.xyz/postGN.php", {m: mid, c: commentId}, function(data, status) {
			myLog(1, "post success.");
		}, "json");

	};

	// 行の集合を受け取り、スタイルを設定する
	const setRowsStyle = function(rows) {
		rows.each(function () {
			setRowStyle( $(this) );
		});
	};

	// 行を受け取り、スタイルを設定する
	const setRowStyle = (row) => {
		const cid = row.find(".r3").html();
		const style = (() => {
			const nikorare = getRealNikorare(cid);
			myLog(3, "set style...", {cid: cid, nikorare: nikorare});

			if (nikorare === undefined || nikorare === 0) {
				return;
	 		}

	 		if (nikorare >= 1 && nikorare < 3) {
				// 黒太文字
				return {fontWeight: "bold"};
			}

			if (nikorare >= 3 && nikorare < 10) {
				// オレンジ太文字
				return {fontWeight: "bold", color: "orange"};
			}

			if (nikorare >= 10) {
				// 赤太文字
				return {fontWeight: "bold", color: "red"};
			}

			throw Error("bad nikorare.");
		})();

		if (style === undefined) {
			return true; // as continue.
		}
		row.find(".r0").css(style);
	}

	// 初期化済みかを判定する
	const hasInitiated = function(_mid) {
		return mid === _mid;
	};

	// ニコるDOM取得
	const getNikoruDOM = (commentId, isNikorared) => {
		return `
		<div class="nikoruTooltip">
			<img id="nikoruKun${commentId}" class="nikoruKun ${isNikorared ? "nikoruKun_disabled" : ""}" src="${nikoruImage_tate}">
			<div class="nikorare">${getRealNikorare(commentId)}</div>
		</div>
		`;
	};

	// ロガー
	const myLog = function(level, ...a) {
		if (level <= logLevel) {
			console.log(...a);
		}
	};

	// コメントの現在ニコられを取得する。サーバーを参照するのではなく、このページでニコられていれば、取得時点の数に1を足すだけ。
	const getRealNikorare = (cid) => {
		const gotNikorare = getNikorare(cid);
		const realNikorare = commentIsNikorared(cid) ? gotNikorare + 1 : gotNikorare;

		// 3桁までに制限する
		return Math.min(realNikorare, maxDisplayNikorare);
	};

	// コメントがニコられていればtrue
	const commentIsNikorared = (cid) => {
		return nikorizumiCIDList.includes(cid);
	};

	// イベントを無効化する
	const killEvent = function(event) {
		event.preventDefault();
		event.stopPropagation();
	};

	// CIDのニコられを取得する
	const getNikorare = (cid) => {
		return myNimj[cid] || 0;
	};

	// 数値をNBSPでパディングして返す
	const padding = (val) => {
	    const len = String(val).length;
	    const neededPadding = 3 - len;
	    let result = val;
	    for (var i = 0; i < neededPadding; i++) {
	        result += "&nbsp;";
	    }
	    return result;
	};

	return my;

})();

// Listen for messages
chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {

	GN.init(msg.movieId);

//    sendResponse(document);

});
